﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBrown
{
    [Serializable()]
    class Battle:ISerializable
    {
        public Pokemon[] Team1;
        public Pokemon[] Team2;
        public double[,] TypeModifier = new double[,]
            /*Normal*/      {{1,1,1,1,1,1,1,1,1,1,0.5,1,1,1,0 },
            /*Fighting*/    {2,1,1,1,0.5,1,0.5,1,1,0.5,2,2,0.5,1,0},
            /*Fire*/        {1,1,0.5,0.5,1,2,1,1,1,1,0.5,2,2,0.5,1}, 
            /*Water*/       {1,1,2,0.5,1,0.5,1,1,2,1,2,1,1,0.5,1},
            /*Flying*/      {1,2,1,1,1,2,1,2,1,1,0.5,1,2,1,1},
            /*Grass*/       {1,1,0.5,2,0.5,0.5,0.5,1,2,1,2,1,0.5,0.5,1},
            /*Poison*/      {1,1,1,1,1,2,0.5,1,0.5,1,0.5,1,2,1,0.5},
            /*Electic*/     {1,1,1,2,2,0.5,1,0.5,0,1,1,1,1,0.5,1},
            /*Ground*/      {1,1,2,1,0,0.5,2,2,1,1,2,1,0.5,1,1},
            /*Psychic*/     {1,2,1,1,1,1,2,1,1,0.5,1,1,1,1,1 }, 
            /*Rock*/        {1,0.5,2,1,2,1,1,1,0.5,1,1,2,2,1,1},
            /*Ice*/         {1,1,1,0.5,2,2,1,1,2,1,1,0.5,1,2,1},
            /*Bug*/         {1,0.5,0.5,1,0.5,2,2,1,1,2,1,1,1,1,0.5},
            /*Dragon*/      {1,1,1,1,1,1,1,1,1,1,1,1,1,2,1},
            /*Ghost*/       {0,1,1,1,1,1,1,1,1,0,1,1,1,1,2} };

        public Turn turn = Turn.MyTurn;

        public Battle(Pokemon[] Team1, Pokemon[] Team2)
        {
            this.Team1 = Team1;
            this.Team2 = Team2;

        }
        // calculation the damage done when a pokemon attacks another pokemon  using the move
        public double DamageCalculation(Pokemon a, Pokemon b, Move c)
        {
            double damage = 0.0;
            if (c.category == PokeCategory.Physical)
            {


                damage = ((((((2 * 50) / 5) + 2) * c.power * (a.Attack / b.Defense)) / 50) + 2) * Modifier(a, b, c);




            }
            else if (c.category == PokeCategory.Status && c.accuracy>0)
            {
       
                b.battleStats[c.statChange] = b.battleStats[c.statChange] * c.power;


            }
            else if (c.category == PokeCategory.Status && c.accuracy < 0)
            {
                a.battleStats[c.statChange] = a.battleStats[c.statChange] * c.power;
            }
            else if (c.category == PokeCategory.Special)
            {

                damage = (((((2 * 50) / 5 + 2) * c.power * (a.Sp_Atk / b.Sp_Def) + 2) / 50)) * Modifier(a, b, c);



            }
            // check to see if the atk will miss or not
            if (HitOrMiss(c) && c.accuracy>0)
            {
                return damage;
            }
            else
            {
                return 0;
            }



        }
        // checks a pokemon and a move and checks if the move is super effective or not very effective or normal
        public double weakness(Move a, Pokemon b)
        {
            double Effective = 0;
            Effective = TypeModifier[(int)a.type, (int)b.type];

            return Effective;
        }
        // adds all the modifiers together used in final damage calculations
        public double Modifier(Pokemon a, Pokemon b, Move c)
        {
            double modify = 0;
            modify = SameType(a, c) * critical() * weakness(c, b);

            return modify;
        }
        // checks if the pokemon atking is of the same type as the move he is using 
        public double SameType(Pokemon a, Move c)
        {
            if (a.type.Equals(c.type))
            {
                //regular formula is 1.5
                return 1.25;
            }
            else
            {
                return 1;
            }
        }
        // checks if the pokemon did a critical hit to enhance its damage
        public double critical()
        {
            double Crit = 0;
            Random rand = new Random();
            double success = rand.Next(100);

            if (success < 10)
            {
                Crit = 1.5;
            }
            else
            {
                Crit = 1;
            }
            return Crit;
        }

        // determines the percent hit rate of a pokemon and return if he hits or not
        public Boolean HitOrMiss(Move a)
        {
            Random rand = new Random();
            int numb = rand.Next(100);


            if (numb < 100 * a.accuracy)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {

            info.AddValue("Team1", Team1);
            info.AddValue("Team2", Team2);

        }
        public Battle(SerializationInfo info, StreamingContext context)
        {
            Team1 = (Pokemon[])info.GetValue("Team1", typeof(Pokemon[]));
            Team2 = (Pokemon[])info.GetValue("Team2", typeof(Pokemon[]));
        }
    }
}
