﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Timers;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace PokemonBrown
{
    /// <summary>
    /// Interaction logic for BattleScene.xaml
    /// </summary>
    [Serializable]
    public partial class BattleScene : Window,ISerializable
    {
        // sets all the variables
        public Pokemon[] User { get; set; }
        public Pokemon[] Enemy { get; set; }
        public int currentUserPokemon;
        public int currentEnemyPokemon;
        public int dead = 0;
        System.Windows.Threading.DispatcherTimer Timer = new System.Windows.Threading.DispatcherTimer();
        Battle battle = null;
        System.Timers.Timer gameTime = new System.Timers.Timer();
        System.Timers.Timer TurnTime = new System.Timers.Timer();
        System.Timers.Timer TurnTime1 = new System.Timers.Timer();
        public int gameTimeSeconds = 10;
        public String[] StringStats = new String[] { "Attack", "HP", "Defense", "Sp_Atk", "Sp_Defense", "Speed", "accuracy" };




        public void PlaceInfo()
        {
            UserPic.Source = new BitmapImage(User[currentUserPokemon].userPic);
            EnemyPic.Source = new BitmapImage(Enemy[currentEnemyPokemon].opponentpic);
            Hp1.Maximum = User[currentUserPokemon].battleStats[7];
            Hp1.Value = User[currentUserPokemon].battleStats[1];
            hpLabel.Content = ((int)User[currentUserPokemon].battleStats[1]).ToString() + "/" + User[currentUserPokemon].battleStats[7];
            Hp2.Maximum = Enemy[currentEnemyPokemon].battleStats[7];
            Hp2.Value = Enemy[currentEnemyPokemon].battleStats[1];
            pokemonName.Content = User[currentUserPokemon].name;
            enemyName.Content = Enemy[currentEnemyPokemon].name;

            Move[] myMoves = User[currentUserPokemon].moves;

            atk1.Header = myMoves[0].name;
            atk2.Header = myMoves[1].name;
            atk3.Header = myMoves[2].name;
            atk4.Header = myMoves[3].name;
        }
        // initializes all the components taht are added into the start of the battle screen. pokemon images name hp and stats
        public BattleScene(Pokemon[] userpokemons, Pokemon[] enemypokemons)
        {
            InitializeComponent();
            this.User = userpokemons;
            this.Enemy = enemypokemons;
            Timer.Tick += DispatcherTimer_Tick;
            Timer.Interval = new TimeSpan(0, 0, 1);
            Timer.Start();

            PlaceInfo();

            battle = new Battle(userpokemons, enemypokemons);
            gameTime.Interval = 1000;
            gameTime.Elapsed += GameTimeSeconds_Elapsed;
            gameTime.Enabled = true;
            TurnTime.Interval = 2000;
            TurnTime.Elapsed += TurnTimeSeconds_Elapsed;
            TurnTime1.Interval = 2000;
            TurnTime.Elapsed += Turn1TimeSeconds_Elapsed;
        }

        public BattleScene(Pokemon[] userpokemons, Pokemon[] enemypokemons, int currentuserpokemon, int currentenemypokemon, bool shouldAttack)
        {
            InitializeComponent();
            User = userpokemons;
            Enemy = enemypokemons;
            Timer.Tick += DispatcherTimer_Tick;
            Timer.Interval = new TimeSpan(0, 0, 1);
            Timer.Start();
            this.currentUserPokemon = currentuserpokemon;
            this.currentEnemyPokemon = currentenemypokemon;

            PlaceInfo();

            battle = new Battle(User, Enemy);

            gameTime.Interval = 1000;
            gameTime.Elapsed += GameTimeSeconds_Elapsed;
            gameTime.Enabled = true;
            TurnTime.Interval = 2000;
            TurnTime.Elapsed += TurnTimeSeconds_Elapsed;
            TurnTime1.Interval = 2000;
            TurnTime.Elapsed += Turn1TimeSeconds_Elapsed;
            if (shouldAttack)
            {
                PokeMenu.IsEnabled = false;
                TurnTime.Enabled = true;
            }
            
        }
        // so taht if we need to stop for a moment
        private void Turn1TimeSeconds_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.Invoke(() =>{
               
                TurnTime1.Enabled = false;
                
            });
        }
        // what happens when the turn timer ends. to handle the turns
        private void TurnTimeSeconds_Elapsed(object sender, ElapsedEventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                
                
                EnemyMove();
                gameTime.Enabled = true;
                TurnTime.Enabled = false;
                PokeMenu.IsEnabled = true;



            });
        }
        // what happens after the user takes too long to play his turn
        private void GameTimeSeconds_Elapsed(object sender, ElapsedEventArgs e)
        {

            this.Dispatcher.Invoke(() =>
            {
                
                if (gameTimeSeconds != 0)
                {
                    gameTimeSeconds--;
                    fightMessage.Text = "You have " + gameTimeSeconds.ToString() + "s" + " to play your turn ";
                    PokeMenu.IsEnabled = true;
                }
               else
                {
                    
                    EnemyMove();
                    gameTimeSeconds = 10;
                    



                }
            });
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            
        }

        private void Fight_Click(object sender, RoutedEventArgs e)
        {
            PokeMenu.Visibility = Visibility.Hidden;
            AtkMenu.Visibility = Visibility.Visible;
        }
     

        private void Surrender_Click(object sender, RoutedEventArgs e)
        {
            gameTime.Stop();
            TurnTime.Stop();
            TurnTime1.Stop();
            System.Windows.Application.Current.Shutdown();
        }

        

        private void Switch_Click(object sender, RoutedEventArgs e)
        {
            Switch_poke(true);
           
        }
        // switches the pokemons in the switch selection screen
        private void Switch_poke(bool shouldAttack)
        {
            gameTime.Enabled = false;
            TurnTime.Enabled = false;
            PokeMenu.IsEnabled = false;
            SwitchPokemon selectionWindow = new SwitchPokemon(User, Enemy, currentUserPokemon, currentEnemyPokemon, shouldAttack);
            this.Visibility = Visibility.Hidden;
            selectionWindow.ShowDialog();
            this.Close();
            gameTime.Enabled = true;
        }
        // saves the game state
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            Stream stream = File.Open("Gamestate.dat", FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            BattleScene gamestate = new BattleScene(User, Enemy, currentUserPokemon, currentEnemyPokemon, false);
            bf.Serialize(stream, gamestate);
            stream.Close();
        }

        private void ProgressBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
                       

        }
        // checks which pokemon has the highest speed
        private Boolean Speed(Pokemon a,Pokemon b)
        {
            if (a.battleStats[5] > b.battleStats[5])
            {
                return true;
            }
            else
            {
                return false;
            }

            
        }
        // when you click to atk what happens
        private void AttackClick(object sender, RoutedEventArgs e)
        {
           // selects all the moces of the current pokemon and adds them to the menu
            Move selectedMove = User[currentUserPokemon].moves[int.Parse(((MenuItem)sender).Uid)];
            
            // checks who starts by checking who is faster
            if (Speed(User[currentUserPokemon], Enemy[currentEnemyPokemon]))
            {
                // checks if the enemy pokemon is alive
                if (Enemy[currentEnemyPokemon].battleStats[1] > 0)
                {
                    TurnTime.Enabled = true;
                    gameTime.Enabled = false;
                }
                // if dead switches pokemon
                else
                {
                    fightMessage.Text = "enemy has fainted";
                    LoadNextEnemyPokemon();
                    TurnTime1.Enabled = true;
                    
                    
                }

              // checks if there is enough power points to use the move
                if (selectedMove.pp > 0)
                {

                    selectedMove.pp -= 1;
                    // deals damage
                    double dmg = battle.DamageCalculation(User[currentUserPokemon], Enemy[currentEnemyPokemon], selectedMove);
                    // checks what the nam of the move is and the type of it to see what text should be displayed
                    if (battle.weakness(selectedMove, Enemy[currentEnemyPokemon]) > 1 && battle.HitOrMiss(selectedMove))
                    {
                        fightMessage.Text = User[currentUserPokemon].name + " used " + selectedMove.name + " Its super effective";

                    }
                    else if (battle.weakness(selectedMove, Enemy[currentEnemyPokemon]) < 1 && battle.HitOrMiss(selectedMove))
                    {
                        fightMessage.Text = User[currentUserPokemon].name + " used " + selectedMove.name + " Its not very effective";
                    }
                    else if (!battle.HitOrMiss(selectedMove) && selectedMove.accuracy > 0)
                    {
                        fightMessage.Text = User[currentUserPokemon].name + " used " + selectedMove.name + " " + selectedMove.name + " Missed";
                    }
                    else if (selectedMove.category == PokeCategory.Status && selectedMove.accuracy <= 0)
                    {
                        fightMessage.Text = User[currentUserPokemon].name + " used " + selectedMove.name + " Raised " + User[currentUserPokemon].name + " " + StringStats[selectedMove.statChange];
                    }
                    else if (selectedMove.category == PokeCategory.Status && selectedMove.accuracy > 0 && battle.HitOrMiss(selectedMove))
                    {
                        fightMessage.Text = User[currentUserPokemon].name + " used " + selectedMove.name + " Decreased " + Enemy[currentEnemyPokemon].name +" "+ StringStats[selectedMove.statChange];
                    }
                    else
                    {
                        fightMessage.Text = User[currentUserPokemon].name + " used " + selectedMove.name;
                    }
                    // sets the new variables
                   
                    gameTimeSeconds = 10;
                    Enemy[currentEnemyPokemon].battleStats[1] -= dmg;
                    Hp2.Value = Enemy[currentEnemyPokemon].battleStats[1];


                    PokeMenu.Visibility = Visibility.Visible;
                    AtkMenu.Visibility = Visibility.Hidden;
                }
                // if no more pp
                else
                {
                    MessageBox.Show("Not enough pp use another move");
                }
                // checks if enemy is alive
                if (Enemy[currentEnemyPokemon].battleStats[1] > 0)
                {
                    TurnTime.Enabled = true;
                    gameTime.Enabled = false;
                }
                else
                {
                    
                    fightMessage.Text = "enemy has fainted";
                    LoadNextEnemyPokemon();
                    //TurnTime1.Enabled = true;
                }

            }
            // same code but for if the user starts
            else
            {
                
                if (selectedMove.pp > 0)
                {

                    selectedMove.pp -= 1;
                    double dmg = battle.DamageCalculation(User[currentUserPokemon], Enemy[currentEnemyPokemon], selectedMove);
                    if (battle.weakness(selectedMove, Enemy[currentEnemyPokemon]) > 1 && battle.HitOrMiss(selectedMove))
                    {
                        fightMessage.Text = User[currentUserPokemon].name +" used " +selectedMove.name + " Its super effective";

                    }
                    else if (battle.weakness(selectedMove, Enemy[currentEnemyPokemon]) < 1 && battle.HitOrMiss(selectedMove))
                    {
                        fightMessage.Text = User[currentUserPokemon].name + " used " + selectedMove.name + " Its not very effective";
                    }
                    else if (!battle.HitOrMiss(selectedMove) && selectedMove.accuracy>0)
                    {
                       fightMessage.Text = User[currentUserPokemon].name + " used " + selectedMove.name +" " +selectedMove.name + " Missed";
                    }
                    else if (selectedMove.category == PokeCategory.Status && selectedMove.accuracy <= 0)
                    {
                        fightMessage.Text = User[currentUserPokemon].name + " used " + selectedMove.name + " Raised "+ User[currentUserPokemon].name+ " "+ StringStats[selectedMove.statChange];
                    }
                    else if (selectedMove.category == PokeCategory.Status && selectedMove.accuracy > 0 && battle.HitOrMiss(selectedMove))
                    {
                        fightMessage.Text = User[currentUserPokemon].name + " used " + selectedMove.name + " Decreased "+ Enemy[currentEnemyPokemon].name +" " + StringStats[selectedMove.statChange] ;
                    }
                    else
                    {
                        fightMessage.Text = User[currentUserPokemon].name + " used " + selectedMove.name;
                    }
                    gameTimeSeconds = 10;
                    Enemy[currentEnemyPokemon].battleStats[1] -= dmg;
                    Hp2.Value = Enemy[currentEnemyPokemon].battleStats[1];


                    PokeMenu.Visibility = Visibility.Visible;
                    AtkMenu.Visibility = Visibility.Hidden;
                }

                else
                {
                    MessageBox.Show("Not enough pp use another move");
                }
                if (Enemy[currentEnemyPokemon].battleStats[1] > 0)
                {
                    TurnTime.Enabled = true;
                    gameTime.Enabled = false;
                }
                else
                {
                    
                    fightMessage.Text = "enemy has fainted";
                    LoadNextEnemyPokemon();
                    TurnTime1.Enabled = true;
                }
               
            }
            PokeMenu.IsEnabled = false;
        }

       
        // loads teh next enemy pokemon and if there isnt any you win
        private void LoadNextEnemyPokemon()
        {
            currentEnemyPokemon++;
            if(currentEnemyPokemon >= Enemy.Length)
            {
                MessageBox.Show("You win");
                SelectionWin selection = new SelectionWin();
                this.Visibility = Visibility.Hidden;
                gameTime.Enabled = false;
                TurnTime.Enabled = false;
                TurnTime1.Enabled = false;
                selection.ShowDialog();
                this.Close();
                return;
            }

            PlaceInfo();


        }
        public Move BestMove(Move[] moves, Pokemon a,Pokemon b)
        {
            Move Strong = moves[0];
            for (int i =0; i<moves.Length-1; i++)
            {
                for (int j = 1; j< moves.Length; j++)
                {
                    if (battle.DamageCalculation(a,b,moves[i]) > battle.DamageCalculation(a, b, moves[j]))
                    {
                        Strong = moves[i];
                    }
                }
            }
            return Strong;

        }
        // what happens when its the enemies turn
        private void EnemyMove()
        {


            // enemy turn so put the fight menu back on
            PokeMenu.IsEnabled = true;
            // same as above but for enemy

            if (User[currentUserPokemon].battleStats[1] > 0)
            {
                Move selectedMove = BestMove(Enemy[currentEnemyPokemon].moves,Enemy[currentEnemyPokemon], User[currentUserPokemon]);
                double dmg = battle.DamageCalculation(Enemy[currentEnemyPokemon], User[currentUserPokemon], selectedMove);

                if (battle.weakness(selectedMove, User[currentUserPokemon]) > 1 && battle.HitOrMiss(selectedMove))
                {
                    fightMessage.Text = Enemy[currentEnemyPokemon].name + " used " + selectedMove.name + " Its super effective";

                }
                else if (battle.weakness(selectedMove, User[currentUserPokemon]) < 1 && battle.HitOrMiss(selectedMove))
                {
                    fightMessage.Text = Enemy[currentEnemyPokemon].name + " used " + selectedMove.name + " Its not very effective";
                }
                else if (!battle.HitOrMiss(selectedMove) && selectedMove.accuracy > 0)
                {
                    fightMessage.Text = Enemy[currentEnemyPokemon].name + " used " + selectedMove.name + " " + selectedMove.name + " Missed";
                }
                else if (selectedMove.category == PokeCategory.Status && selectedMove.accuracy <= 0)
                {
                    fightMessage.Text = Enemy[currentEnemyPokemon].name + " used " + selectedMove.name + " Raised " + Enemy[currentEnemyPokemon].name + " " + StringStats[selectedMove.statChange];
                }
                else if (selectedMove.category == PokeCategory.Status && selectedMove.accuracy > 0 && battle.HitOrMiss(selectedMove))
                {
                    fightMessage.Text = Enemy[currentEnemyPokemon].name + " used " + selectedMove.name + " Decreased " + User[currentUserPokemon] + " " + StringStats[selectedMove.statChange];
                }
                else
                {
                    fightMessage.Text = Enemy[currentEnemyPokemon].name + " used " + selectedMove.name;
                }
                User[currentUserPokemon].battleStats[1] -= dmg;
                Hp1.Value = User[currentUserPokemon].battleStats[1];

                int hp = (int)Math.Round(User[currentUserPokemon].battleStats[1]);

                if (hp < 0)
                {
                    hp = 0;

                }
                hpLabel.Content = hp.ToString() + "/" + User[currentUserPokemon].battleStats[7].ToString();
            }
            if (User[currentUserPokemon].battleStats[1] < 1)
            {


                fightMessage.Text = User[currentUserPokemon].name + " pokemon has fainted";
                dead++;
                    if (dead == 6)
                {
                    MessageBox.Show("You lose");
                    SelectionWin selection = new SelectionWin();
                    this.Visibility = Visibility.Hidden;
                    gameTime.Enabled = false;
                    TurnTime.Enabled = false;
                    TurnTime1.Enabled = false;
                    selection.ShowDialog();                
                    this.Close();
                }
                Switch_poke(false);
            }
            
            
            
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("User", User);
            info.AddValue("Enemy", Enemy);
            info.AddValue("currentUserPokemon", currentUserPokemon);
            info.AddValue("currentEnemyPokemon", currentEnemyPokemon);
            info.AddValue("gameTimeSeconds", gameTimeSeconds);
            info.AddValue("StringStats", StringStats);

        }
        public BattleScene(SerializationInfo info, StreamingContext context)
        {
            User = (Pokemon[])info.GetValue("User", typeof(Pokemon[]));
            Enemy = (Pokemon[])info.GetValue("Enemy", typeof(Pokemon[]));
            currentUserPokemon = (int)info.GetValue("currentUserPokemon", typeof(int));
            currentEnemyPokemon = (int)info.GetValue("currentEnemyPokemon", typeof(int));
            gameTimeSeconds = (int)info.GetValue("gameTimeSeconds", typeof(int));
            StringStats = (String[])info.GetValue("StringStats", typeof(String[]));
        }

        private void cheat_Click(object sender, RoutedEventArgs e)
        {
            SwitchPokemon cheatwindow = new SwitchPokemon(Enemy);
            cheatwindow.Show();
        }

        internal Battle Battle1
        {
            get => default(Battle);
            set
            {
            }
        }

        public Turn Turn
        {
            get => default(Turn);
            set
            {
            }
        }
    }
}
//https://www.deviantart.com/pokemonbrendan/art/Pokemon-X-Y-BG-Battle-Grass-Rip-409403768 link for background