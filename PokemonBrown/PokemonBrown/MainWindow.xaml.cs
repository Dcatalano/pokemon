﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
namespace PokemonBrown
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        internal PokeReader PokeReader
        {
            get => default(PokeReader);
            set
            {
            }
        }

        private void StartBtn_Click(object sender, RoutedEventArgs e)
        {

            StreamWriter Stream = new StreamWriter(Environment.CurrentDirectory + "\\..\\..\\SaveFile.txt");
            Stream.WriteLine(username.Text);
            Stream.WriteLine("started playing");


            SelectionWin selection = new SelectionWin();
            this.Visibility = Visibility.Hidden;
            selection.ShowDialog();
            this.Close();
     
        }

        private void LoadBtn_Click(object sender, RoutedEventArgs e)
        {

            //BattleScene battleScene = new BattleScene(usersPokemon, RandomTeam());
            Stream stream = File.Open("Gamestate.dat",FileMode.OpenOrCreate, FileAccess.Read);
            BinaryFormatter bf = new BinaryFormatter();
            BattleScene loadedB = (BattleScene) bf.Deserialize(stream);

            BattleScene toShow = new BattleScene(loadedB.User, loadedB.Enemy, loadedB.currentUserPokemon, loadedB.currentEnemyPokemon, false);
            stream.Close();
            toShow.Show();
            this.Visibility = Visibility.Hidden;
            this.Close();
        }
    }
}
