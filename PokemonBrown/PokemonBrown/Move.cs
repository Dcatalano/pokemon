﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBrown
{
    [Serializable]
    public class Move : PokeReader, ISerializable
    {
        public int moveID;
        public String name;
        public PokeType type;
        public PokeCategory category { get; private set; }
        public int pp { get; set; }
        public double power;
        public double accuracy;
        public int statChange = -1;

        public Move(string stringfields)
        {
            string[] fields = stringfields.Split(',');
            moveID = int.Parse(fields[0]);
            name = fields[1];
            type = PokeTools.stringToType(fields[2]);
            category = PokeTools.stringToCategory(fields[3]);
            pp = int.Parse(fields[4].Replace("*", ""));
            switch (category)
            {
                case PokeCategory.Physical:
                case PokeCategory.Special:
                    fields[5] = fields[5].Replace("*", "");
                    power = double.Parse(fields[5].Replace("?", "-1"));
                    fields[6] = fields[6].Replace("*", "");
                    fields[6] = fields[6].Replace("%", "");
                    accuracy = int.Parse(fields[6].Replace("?", "-1"));
                    break;
                case PokeCategory.Status:
                    string[] powerstat = fields[5].Split('-');
                    power = double.Parse(powerstat[0]);
                    statChange = int.Parse(powerstat[1]);
                    fields[6] = fields[6].Replace("*", "");
                    fields[6] = fields[6].Replace("%", "");
                    accuracy = int.Parse(fields[6].Replace("?", "-1"));
                    break;
            }

        }



        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("name", name);
            info.AddValue("type", type);
            info.AddValue("category", category);
            info.AddValue("pp", pp);
            info.AddValue("power", power);
            info.AddValue("accuracy", accuracy);
            info.AddValue("statChange", statChange);
        }
        public Move(SerializationInfo info, StreamingContext context)
        {
            name = (String)info.GetValue("name", typeof(String));
            type = (PokeType)info.GetValue("type", typeof(PokeType));
            category = (PokeCategory)info.GetValue("category", typeof(PokeCategory));
            pp = (int)info.GetValue("pp", typeof(int));
            power = (double)info.GetValue("power", typeof(double));
            accuracy = (double)info.GetValue("accuracy", typeof(double));
            statChange = (int)info.GetValue("statChange", typeof(int));

        }

        public Pokemon Pokemon
        {
            get => default(Pokemon);
            set
            {
            }
        }

        public PokeCategory PokeCategory
        {
            get => default(PokeCategory);
            set
            {
            }
        }

        public PokeType PokeType
        {
            get => default(PokeType);
            set
            {
            }
        }
    }
}
