﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace PokemonBrown
{
    public class PokeReader
    {
        public static string[] PokemonReader()
        {;
            return System.IO.File.ReadAllLines(Environment.CurrentDirectory + "\\..\\..\\Pokedex.csv");
            
        }
        public static string[] MoveReader(){

            return System.IO.File.ReadAllLines(Environment.CurrentDirectory + "\\..\\..\\MoveList.csv");
        }

        public static Pokemon[] GetPokemons(string[] pokemonList)
        {
            Pokemon[] pokeArray = new Pokemon[pokemonList.Length];
            for (int i = 0; i < pokemonList.Length;i++)
            {
                pokeArray[i] = new Pokemon(pokemonList[i]);
            }
            return pokeArray;
        }

        public static Move[] GetMoves(string[] moveList)
        {
            Move[] moveArray = new Move[moveList.Length];
            for (int i = 0; i < moveList.Length; i++)
            {
                moveArray[i] = new Move(moveList[i]);
            }
            return moveArray;
        }
        /// This method will create a 2d array organized by move types
        /// normal moves, fighting moves...so on
        public static List<List<Move>> GetOrganizedMoves(string[] moveList)
        {
            List<List<Move>> moves = new List<List<Move>>();
            for (int j = 0; j < 15; j++)
            {
                moves.Add(new List<Move>());
            }
            for (int i = 0; i < moveList.Length; i++)
            {               
                Move move = new Move(moveList[i]);
                switch (move.type)
                {
                    case PokeType.Normal:

                        moves[(int)PokeType.Normal].Add(move);
                        break;
                    case PokeType.Fire:
                        moves[(int)PokeType.Fire].Add(move);
                        break;
                    case PokeType.Fighting:
                        moves[(int)PokeType.Fighting].Add(move);
                        break;
                    case PokeType.Water:
                        moves[(int)PokeType.Water].Add(move);
                        break;
                    case PokeType.Flying:
                        moves[(int)PokeType.Flying].Add(move);
                        break;
                    case PokeType.Grass:
                        moves[(int)PokeType.Grass].Add(move);
                        break;
                    case PokeType.Poison:
                        moves[(int)PokeType.Poison].Add(move);
                        break;
                    case PokeType.Electric:
                        moves[(int)PokeType.Electric].Add(move);
                        break;
                    case PokeType.Ground:
                        moves[(int)PokeType.Ground].Add(move);
                        break;
                    case PokeType.Psychic:
                        moves[(int)PokeType.Psychic].Add(move);
                        break;
                    case PokeType.Rock:
                        moves[(int)PokeType.Rock].Add(move);
                        break;
                    case PokeType.Ice:
                        moves[(int)PokeType.Ice].Add(move);
                        break; 
                    case PokeType.Bug:
                        moves[(int)PokeType.Bug].Add(move);
                        break; 
                    case PokeType.Dragon:
                        moves[(int)PokeType.Dragon].Add(move);
                        break;
                    case PokeType.Ghost:
                        moves[(int)PokeType.Ghost].Add(move);
                        break; 
                }
            }

            return moves; 
        }

        public static List<List<Pokemon>> GetOrganizedPokemon(string[] pokelist)
        {
            List<List<Pokemon>> pokemons = new List<List<Pokemon>>();
            for (int j = 0; j < 15; j++)
            {
                pokemons.Add(new List<Pokemon>());
            }
            for (int i = 0; i < pokelist.Length; i++)
            {
                Pokemon chosen = new Pokemon(pokelist[i]);
                switch (chosen.type)
                {
                    case PokeType.Normal:

                        pokemons[(int)PokeType.Normal].Add(chosen);
                        break;
                    case PokeType.Fire:
                        pokemons[(int)PokeType.Fire].Add(chosen);
                        break;
                    case PokeType.Fighting:
                        pokemons[(int)PokeType.Fighting].Add(chosen);
                        break;
                    case PokeType.Water:
                        pokemons[(int)PokeType.Water].Add(chosen);
                        break;
                    case PokeType.Flying:
                        pokemons[(int)PokeType.Flying].Add(chosen);
                        break;
                    case PokeType.Grass:
                        pokemons[(int)PokeType.Grass].Add(chosen);
                        break;
                    case PokeType.Poison:
                        pokemons[(int)PokeType.Poison].Add(chosen);
                        break;
                    case PokeType.Electric:
                        pokemons[(int)PokeType.Electric].Add(chosen);
                        break;
                    case PokeType.Ground:
                        pokemons[(int)PokeType.Ground].Add(chosen);
                        break;
                    case PokeType.Psychic:
                        pokemons[(int)PokeType.Psychic].Add(chosen);
                        break;
                    case PokeType.Rock:
                        pokemons[(int)PokeType.Rock].Add(chosen);
                        break;
                    case PokeType.Ice:
                        pokemons[(int)PokeType.Ice].Add(chosen);
                        break;
                    case PokeType.Bug:
                        pokemons[(int)PokeType.Bug].Add(chosen);
                        break;
                    case PokeType.Dragon:
                        pokemons[(int)PokeType.Dragon].Add(chosen);
                        break;
                    case PokeType.Ghost:
                        pokemons[(int)PokeType.Ghost].Add(chosen);
                        break;
                }
            }

            return pokemons;
        }
    }
    
}
