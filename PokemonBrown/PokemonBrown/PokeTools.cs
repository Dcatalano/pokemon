﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokemonBrown
{
    class PokeTools
    {
        internal PokeReader PokeReader
        {
            get => default(PokeReader);
            set
            {
            }
        }

        public static PokeType stringToType(string type)
        {
            switch (type)
            {
                case "Normal":
                    return PokeType.Normal;
                case "Fire":
                    return PokeType.Fire;
                case "Fighting":
                    return PokeType.Fighting;
                case "Water":
                    return PokeType.Water;
                case "Flying":
                    return PokeType.Flying;
                case "Grass":
                    return PokeType.Grass;
                case "Poison":
                    return PokeType.Poison;
                case "Electric":
                    return PokeType.Electric;
                case "Ground":
                    return PokeType.Ground;
                case "Psychic":
                    return PokeType.Psychic;
                case "Rock":
                    return PokeType.Rock;
                case "Ice":
                    return PokeType.Ice;
                case "Bug":
                    return PokeType.Bug;
                case "Dragon":
                    return PokeType.Dragon;
                case "Ghost":
                    return PokeType.Ghost;
            }
            return PokeType.Normal;

        }

        public static PokeCategory stringToCategory(string category)
        {
            switch (category)
            {
                case "Physical":
                    return PokeCategory.Physical;
                case "Status":
                    return PokeCategory.Status;
                case "Special":
                    return PokeCategory.Special;
            }
            return PokeCategory.Status;
        }

        




    }
}
