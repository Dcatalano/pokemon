﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace PokemonBrown
{
    [Serializable()]
    public class Pokemon : ISerializable
    {
        public string copystring;
        public int pokeID;
        public double Attack { get; private set; }
        public double HP { get; private set; } 
        public double Defense { get; private set; }
        public double Sp_Atk { get; private set; }
        public double Sp_Def { get; private set; }
        public double Speed { get; private set; }
        public string name { get; private set; }
        public Uri userPic;
        public Uri opponentpic;
        public string mass;
        public PokeType type { get; private set; }
        public Move[] moves = new Move[4];
        public Double[] battleStats = new Double[8];
        
       
        public Pokemon(String name, double Attack, double HP, double Defense, double Sp_Atk, double Sp_Def, double Speed,PokeType type,string Mass)
        {

            this.name = name;
            this.Attack = Attack;
            this.HP = HP;
            this.Defense = Defense;
            this.Sp_Atk = Sp_Atk;
            this.Sp_Def = Sp_Def;
            this.Speed = Speed;
            this.mass = Mass;
        }

        public void TransferStats()
        {
            battleStats[0] = StatFormula(Attack);
            battleStats[1] = StatFormula(HP)+55;
            battleStats[2] = StatFormula(Defense);
            battleStats[3] = StatFormula(Sp_Atk);
            battleStats[4] = StatFormula(Sp_Def);
            battleStats[5] = StatFormula(Speed);
            battleStats[6] = 100;
            battleStats[7] = StatFormula(HP) + 55;
        }

        public static double StatFormula(double stat)
        {

            return ((((stat + 3) * 2) * 50) / 100) + 5;
        }

        public Pokemon (String stringFields)
        {
            copystring = stringFields;
            string[] fields = stringFields.Split(',');

            pokeID = int.Parse(fields[0]);
            name = fields[1];
            HP = double.Parse(fields[2]);
            Attack = double.Parse(fields[3]);
            Defense = double.Parse(fields[4]);
            Sp_Atk = double.Parse(fields[5]);
            Sp_Def = double.Parse(fields[6]);
            Speed = double.Parse(fields[7]);
            type = PokeTools.stringToType(fields[9]);
            mass = fields[11];
            string opponentPath = fields[13] + fields[14] + fields[15];
            string userPath = fields[16] + fields[17] + fields[18];
            userPic = new Uri(userPath, UriKind.Relative);
            opponentpic = new Uri(opponentPath, UriKind.Relative);

        }
        public Pokemon Copy()
        {
            Pokemon copied = new Pokemon(this.copystring);
            copied.moves = this.moves;
            return copied;

        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("copystring", copystring);
            info.AddValue("pokeID", pokeID);
            info.AddValue("Attack",Attack);
            info.AddValue("HP",HP);
            info.AddValue("Defense",Defense);
            info.AddValue("Sp_Atk", Sp_Atk);
            info.AddValue("Sp_Def", Sp_Def);
            info.AddValue("Speed",Speed);
            info.AddValue("name",name);
            info.AddValue("userPic",userPic);
            info.AddValue("opponentpic",opponentpic);
            info.AddValue("mass",mass);
            info.AddValue("type",type);
            info.AddValue("moves",moves);
            info.AddValue("battleStats",battleStats);
    }
        public Pokemon(SerializationInfo info, StreamingContext context)
        {
            copystring= name = (string)info.GetValue("copystring", typeof(string));
            pokeID =(int)info.GetValue("pokeID", typeof(int));
            Attack = (double)info.GetValue("Attack", typeof(double));
            HP = (double)info.GetValue("HP", typeof(double));
            Defense = (double)info.GetValue("Defense", typeof(double));
            Sp_Atk = (double)info.GetValue("Sp_Atk", typeof(double));
            Sp_Def= (double)info.GetValue("Sp_Def", typeof(double));
            Speed = (double)info.GetValue("Speed", typeof(double));
            name = (string)info.GetValue("name", typeof(string));
            userPic = (Uri)info.GetValue("userPic", typeof(Uri));
            opponentpic = (Uri)info.GetValue("opponentpic", typeof(Uri));
            mass = (string)info.GetValue("mass", typeof(string));
            type = (PokeType)info.GetValue("type", typeof(PokeType));
            moves = (Move[])info.GetValue("moves", typeof(Move[])); 
            battleStats = (double[])info.GetValue("battleStats", typeof(double[]));
        }

        public PokeType PokeType
        {
            get => default(PokeType);
            set
            {
            }
        }

        internal PokeReader PokeReader
        {
            get => default(PokeReader);
            set
            {
            }
        }

        public SelectionWin SelectionWin
        {
            get => default(SelectionWin);
            set
            {
            }
        }
    }
}
