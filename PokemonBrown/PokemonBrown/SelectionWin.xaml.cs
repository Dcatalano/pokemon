﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PokemonBrown
{ 
    /// <summary>
    /// Interaction logic for SelectionWin.xaml
    /// </summary>
    /// 
    public partial class SelectionWin : Window
    {
        private int filled = 0;
        ObservableCollection<StackPanel> pokeImages = new ObservableCollection<StackPanel>();
        Pokemon[] pokemons = PokeReader.GetPokemons(PokeReader.PokemonReader());
        List<List<Move>> moves = PokeReader.GetOrganizedMoves(PokeReader.MoveReader());
        List<List<Pokemon>> organizedPokemon = PokeReader.GetOrganizedPokemon(PokeReader.PokemonReader());
        Random rnd = new Random();
        Pokemon[] usersPokemon = new Pokemon[6];

        public SelectionWin()
        {

            InitializeComponent();
            Init();
        }

        public SelectionWin(Pokemon[] userPokemons)
        {
            InitializeComponent();
        }

        public BattleScene BattleScene
        {
            get => default(BattleScene);
            set
            {
            }
        }

        private void FightBtn_Click(object sender, RoutedEventArgs e)
        {
            BattleScene battleScene = new BattleScene(usersPokemon, typedTeam());
            this.Visibility = Visibility.Hidden;
            battleScene.ShowDialog();
            
        }


        public void Init()
        {
           
            Image[] listo = new Image[pokemons.Length];
            int columnsize = pokemons.Length / 3;
            for (int i = 0; i < pokemons.Length; i++)
            {
                StackPanel stack = new StackPanel();
                listo[i] = new Image();
                listo[i].Source = new BitmapImage(pokemons[i].userPic);
                listo[i].Height = 50;
                listo[i].Width = 50;
                Button name = new Button( );
                name.Content = pokemons[i].pokeID + " " + pokemons[i].name;
                name.HorizontalAlignment = HorizontalAlignment.Center;
                name.IsEnabled = true;
                stack.Children.Add(listo[i]);
                stack.Children.Add(name);
                name.AddHandler(Button.ClickEvent, new RoutedEventHandler(pokemonselected));
                pokeImages.Add(stack);
                
            }

            PokeList.ItemsSource = pokeImages;
        }

      

        private void pokemonselected(object sender, RoutedEventArgs e)
        {

            Button selected = (Button)sender;
            int id =int.Parse((((string)selected.Content).Split(' '))[0]);
            FillBox(id);
        }
        
        private void FillBox(int id)
        {
            Pokemon chosen = pokemons[id - 1];
            switch (filled)
            {
                case 0:
                    Front1.Source = new BitmapImage(chosen.opponentpic);
                    Back1.Source = new BitmapImage(chosen.userPic);
                    hp1.Text = "HP=" + chosen.HP.ToString();
                    def1.Text = "DEF=" + chosen.Defense.ToString();
                    atk1.Text = "ATK=" + chosen.Attack.ToString();
                    spak1.Text = "SPAK=" + chosen.Sp_Atk.ToString();
                    spd1.Text = "SPDF=" + chosen.Sp_Def.ToString();
                    speed1.Text = "SPD=" + chosen.Speed.ToString();
                    usersPokemon[filled] = BattlePrep(pokemons[id - 1]);
                    move11.Text = chosen.moves[0].name;
                    move12.Text = chosen.moves[1].name;
                    move13.Text = chosen.moves[2].name;
                    move14.Text = chosen.moves[3].name;
                    filled++;
                    break;

                case 1:
                    Front2.Source = new BitmapImage(chosen.opponentpic);
                    Back2.Source = new BitmapImage(chosen.userPic);
                    hp2.Text = "HP=" + chosen.HP.ToString();
                    def2.Text = "DEF=" + chosen.Defense.ToString();
                    atk2.Text = "ATK=" + chosen.Attack.ToString();
                    spak2.Text = "SPAK=" + chosen.Sp_Atk.ToString();
                    spd2.Text = "SPDF=" + chosen.Sp_Def.ToString();
                    speed2.Text = "SPD=" + chosen.Speed.ToString();
                    usersPokemon[filled] = BattlePrep(pokemons[id - 1]);
                    move21.Text = chosen.moves[0].name;
                    move22.Text = chosen.moves[1].name;
                    move23.Text = chosen.moves[2].name;
                    move24.Text = chosen.moves[3].name;
                    filled++;
                    break;
                case 2:
                    Front3.Source = new BitmapImage(chosen.opponentpic);
                    Back3.Source = new BitmapImage(chosen.userPic);
                    hp3.Text = "HP=" + chosen.HP.ToString();
                    def3.Text = "DEF=" + chosen.Defense.ToString();
                    atk3.Text = "ATK=" + chosen.Attack.ToString();
                    spak3.Text = "SPAK=" + chosen.Sp_Atk.ToString();
                    spd3.Text = "SPDF=" + chosen.Sp_Def.ToString();
                    speed3.Text = "SPD=" + chosen.Speed.ToString();
                    usersPokemon[filled] = BattlePrep(pokemons[id - 1]);
                    move31.Text = chosen.moves[0].name;
                    move32.Text = chosen.moves[1].name;
                    move33.Text = chosen.moves[2].name;
                    move34.Text = chosen.moves[3].name;
                    filled++;
                    break;
                case 3:
                    Front4.Source = new BitmapImage(chosen.opponentpic);
                    Back4.Source = new BitmapImage(chosen.userPic);
                    hp4.Text = "HP=" + chosen.HP.ToString();
                    def4.Text = "DEF=" + chosen.Defense.ToString();
                    atk4.Text = "ATK=" + chosen.Attack.ToString();
                    spak4.Text = "SPAK=" + chosen.Sp_Atk.ToString();
                    spd4.Text = "SPDF=" + chosen.Sp_Def.ToString();
                    speed4.Text = "SPD=" + chosen.Speed.ToString();
                    usersPokemon[filled] = BattlePrep(pokemons[id - 1]);
                    move41.Text = chosen.moves[0].name;
                    move42.Text = chosen.moves[1].name;
                    move43.Text = chosen.moves[2].name;
                    move44.Text = chosen.moves[3].name;
                    filled++;
                    break;
                case 4:
                    Front5.Source = new BitmapImage(chosen.opponentpic);
                    Back5.Source = new BitmapImage(chosen.userPic);
                    hp5.Text = "HP=" + chosen.HP.ToString();
                    def5.Text = "DEF=" + chosen.Defense.ToString();
                    atk5.Text = "ATK=" + chosen.Attack.ToString();
                    spak5.Text = "SPAK=" + chosen.Sp_Atk.ToString();
                    spd5.Text = "SPDF=" + chosen.Sp_Def.ToString();
                    speed5.Text = "SPD=" + chosen.Speed.ToString();
                    usersPokemon[filled] = BattlePrep(pokemons[id - 1]);
                    move51.Text = chosen.moves[0].name;
                    move52.Text = chosen.moves[1].name;
                    move53.Text = chosen.moves[2].name;
                    move54.Text = chosen.moves[3].name;

                    filled++;
                    break;
                case 5:
                    Front6.Source = new BitmapImage(chosen.opponentpic);
                    Back6.Source = new BitmapImage(chosen.userPic);
                    hp6.Text = "HP=" + chosen.HP.ToString();
                    def6.Text = "DEF=" + chosen.Defense.ToString();
                    atk6.Text = "ATK=" + chosen.Attack.ToString();
                    spak6.Text = "SPAK=" + chosen.Sp_Atk.ToString();
                    spd6.Text = "SPDF=" + chosen.Sp_Def.ToString();
                    speed6.Text = "SPD=" + chosen.Speed.ToString();
                    usersPokemon[filled] = BattlePrep(pokemons[id - 1]);
                    move61.Text = chosen.moves[0].name;
                    move62.Text = chosen.moves[1].name;
                    move63.Text = chosen.moves[2].name;
                    move64.Text = chosen.moves[3].name;
                    filled = 0;
                    break;
             
            }
        }
        private Pokemon BattlePrep(Pokemon p)
        {
            p = p.Copy();
            int pokemonType = (int)p.type;
            int normalAmount = moves[0].Count;
            int typeMovesAmount = moves[pokemonType].Count;
            int mv1 = rnd.Next(0, typeMovesAmount - 1), mv2 = rnd.Next(0, typeMovesAmount - 1);
            int mv3 = rnd.Next(0, normalAmount - 1), mv4 = rnd.Next(0, normalAmount - 1);

            while (mv2 == mv1)
            {
               mv2 = rnd.Next(0, typeMovesAmount - 1);
            }
            while (mv4 == mv3)
            {
                mv4 = rnd.Next(0, normalAmount - 1);
            }
            p.moves[0] = moves[pokemonType][mv1];
            p.moves[1] = moves[pokemonType][mv2];
            p.moves[2] = moves[0][mv3];
            p.moves[3] = moves[0][mv4];
            p.TransferStats();

            return p;
        }
        private Pokemon[] RandomTeam()
        {
            Pokemon[] enemy = new Pokemon[6];
            int max = pokemons.Length-1;
            for (int i = 0; i < 6; i++)
            {
                enemy[i] = BattlePrep( pokemons[rnd.Next(0, max)]);
            }
            return enemy;
        }

        private Pokemon[] typedTeam()
        {
            Pokemon[] enemy = new Pokemon[6];
            int[] counts = new int[6];
            counts[0] = rnd.Next(0, 3);
            counts[1] = rnd.Next(3, 5);
            counts[2] = rnd.Next(5, 7);
            counts[3] = rnd.Next(7, 9);
            counts[4] = rnd.Next(9, 12);
            counts[5] = rnd.Next(12, 14);
            int max = 0;
            for (int j = 0; j < 6; j++)
            {
               max = organizedPokemon[counts[j]].Count;
               enemy[j] = BattlePrep(organizedPokemon[counts[j]][rnd.Next(0, max)]);
            }
            return enemy;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
   
            MainWindow main = new MainWindow();
            this.Visibility = Visibility.Hidden;
            main.Show();
        }


        private void RandomizeBtn_Click(object sender, RoutedEventArgs e)
        {
           Pokemon[] randomTeam = RandomTeam();
            filled = 0;
            for (int i = 0; i < 6; i++)
            {
                FillBox(randomTeam[i].Copy().pokeID);
            }

        }

        private void Typed_Team_Click(object sender, RoutedEventArgs e)
        {
            Pokemon[] randomTeam = typedTeam();
            filled = 0;
            for (int i = 0; i < 6; i++)
            {
                FillBox(randomTeam[i].Copy().pokeID);
            }
        }
    }
}
