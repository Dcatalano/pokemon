﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace PokemonBrown
{
    /// <summary>
    /// Interaction logic for SwitchPokemon.xaml
    /// </summary>
    public partial class SwitchPokemon : Window,ISerializable

        
    {
        ObservableCollection<StackPanel> pokeImages = new ObservableCollection<StackPanel>();
        Pokemon[] pokemons, sendback;
        int user, sendBack2;
        bool shouldattack;
        
        public SwitchPokemon(Pokemon[] Pokemons, Pokemon[] sendBack1, int user, int sendBack2, bool shouldAttack)
        {
            InitializeComponent();
            pokemons = Pokemons;
            sendback = sendBack1;
            this.user = user;
            this.sendBack2 = sendBack2;
            this.shouldattack = shouldAttack;
            FillBox(pokemons);

        }

        public SwitchPokemon(Pokemon[] cheatmode)
        {
            InitializeComponent();
            FillBox(cheatmode);
            pick1.IsEnabled = false;
            pick2.IsEnabled = false;
            pick3.IsEnabled = false;
            pick4.IsEnabled = false;
            pick5.IsEnabled = false;
            pick6.IsEnabled = false;
            Back.IsEnabled = false;

        }

        public void FillBox(Pokemon[] pokemons)
        {
            Poke1.Source = new BitmapImage(pokemons[0].opponentpic);
            Poke2.Source = new BitmapImage(pokemons[1].opponentpic);
            Poke3.Source = new BitmapImage(pokemons[2].opponentpic);
            Poke4.Source = new BitmapImage(pokemons[3].opponentpic);
            Poke5.Source = new BitmapImage(pokemons[4].opponentpic);
            Poke6.Source = new BitmapImage(pokemons[5].opponentpic);

            nameHp1.Text = pokemons[0].name + " " + ((int)pokemons[0].battleStats[1]).ToString() + "/" + pokemons[0].battleStats[7];
            Hp1.Maximum = pokemons[0].battleStats[7];
            Hp1.Value = pokemons[0].battleStats[1];
            def1.Text = "DEF=" + pokemons[0].battleStats[2].ToString();
            atk1.Text = "ATK=" + pokemons[0].battleStats[0].ToString();
            spak1.Text = "SPAK=" + pokemons[0].battleStats[3].ToString();
            spd1.Text = "SPDF=" + pokemons[0].battleStats[4].ToString();
            speed1.Text = "SPD=" + pokemons[0].battleStats[5].ToString();
            move11.Text = pokemons[0].moves[0].name;
            move12.Text = pokemons[0].moves[1].name;
            move13.Text = pokemons[0].moves[2].name;
            move14.Text = pokemons[0].moves[3].name;

            nameHp2.Text = pokemons[1].name + " " + ((int)pokemons[1].battleStats[1]).ToString() + "/" + pokemons[1].battleStats[7];
            Hp2.Maximum = pokemons[1].battleStats[7];
            Hp2.Value = pokemons[1].battleStats[1];
            def2.Text = "DEF=" + pokemons[1].battleStats[2].ToString();
            atk2.Text = "ATK=" + pokemons[1].battleStats[0].ToString();
            spak2.Text = "SPAK=" + pokemons[1].battleStats[3].ToString();
            spd2.Text = "SPDF=" + pokemons[1].battleStats[4].ToString();
            speed2.Text = "SPD=" + pokemons[1].battleStats[5].ToString();
            move21.Text = pokemons[1].moves[0].name;
            move22.Text = pokemons[1].moves[1].name;
            move23.Text = pokemons[1].moves[2].name;
            move24.Text = pokemons[1].moves[3].name;

            nameHp3.Text = pokemons[2].name + " " + ((int)pokemons[2].battleStats[1]).ToString() + "/" + pokemons[2].battleStats[7];
            Hp3.Maximum = pokemons[2].battleStats[7];
            Hp3.Value = pokemons[2].battleStats[1];
            def3.Text = "DEF=" + pokemons[2].battleStats[2].ToString();
            atk3.Text = "ATK=" + pokemons[2].battleStats[0].ToString();
            spak3.Text = "SPAK=" + pokemons[2].battleStats[3].ToString();
            spd3.Text = "SPDF=" + pokemons[2].battleStats[4].ToString();
            speed3.Text = "SPD=" + pokemons[2].battleStats[5].ToString();
            move31.Text = pokemons[2].moves[0].name;
            move32.Text = pokemons[2].moves[1].name;
            move33.Text = pokemons[2].moves[2].name;
            move34.Text = pokemons[2].moves[3].name;


            nameHp4.Text = pokemons[3].name + " " + ((int)pokemons[3].battleStats[1]).ToString() + "/" + pokemons[3].battleStats[7];
            Hp4.Maximum = pokemons[3].battleStats[7];
            Hp4.Value = pokemons[3].battleStats[1];
            def4.Text = "DEF=" + pokemons[3].battleStats[2].ToString();
            atk4.Text = "ATK=" + pokemons[3].battleStats[0].ToString();
            spak4.Text = "SPAK=" + pokemons[3].battleStats[3].ToString();
            spd4.Text = "SPDF=" + pokemons[3].battleStats[4].ToString();
            speed4.Text = "SPD=" + pokemons[3].battleStats[5].ToString();
            move41.Text = pokemons[3].moves[0].name;
            move42.Text = pokemons[3].moves[1].name;
            move43.Text = pokemons[3].moves[2].name;
            move44.Text = pokemons[3].moves[3].name;

            nameHp5.Text = pokemons[4].name + " " + ((int)pokemons[4].battleStats[1]).ToString() + "/" + pokemons[4].battleStats[7];
            Hp5.Maximum = pokemons[4].battleStats[7];
            Hp5.Value = pokemons[4].battleStats[1];
            def5.Text = "DEF=" + pokemons[4].battleStats[2].ToString();
            atk5.Text = "ATK=" + pokemons[4].battleStats[0].ToString();
            spak5.Text = "SPAK=" + pokemons[4].battleStats[3].ToString();
            spd5.Text = "SPDF=" + pokemons[4].battleStats[4].ToString();
            speed5.Text = "SPD=" + pokemons[4].battleStats[5].ToString();
            move51.Text = pokemons[4].moves[0].name;
            move52.Text = pokemons[4].moves[1].name;
            move53.Text = pokemons[4].moves[2].name;
            move54.Text = pokemons[4].moves[3].name;


            nameHp6.Text = pokemons[5].name + " " + ((int)pokemons[5].battleStats[1]).ToString() + "/" + pokemons[5].battleStats[7];
            Hp6.Maximum = pokemons[5].battleStats[7];
            Hp6.Value = pokemons[5].battleStats[1];
            def6.Text = "DEF=" + pokemons[5].battleStats[2].ToString();
            atk6.Text = "ATK=" + pokemons[5].battleStats[0].ToString();
            spak6.Text = "SPAK=" + pokemons[5].battleStats[3].ToString();
            spd6.Text = "SPDF=" + pokemons[5].battleStats[4].ToString();
            speed6.Text = "SPD=" + pokemons[5].battleStats[5].ToString();
            move61.Text = pokemons[5].moves[0].name;
            move62.Text = pokemons[5].moves[1].name;
            move63.Text = pokemons[5].moves[2].name;
            move64.Text = pokemons[5].moves[3].name;

        }
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("pokemons", pokemons);
            info.AddValue("sendback", sendback);
            info.AddValue("sendback2", sendBack2);
            info.AddValue("user", user);
            info.AddValue("shouldattack", shouldattack);
           
        
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            BattleScene battleScene = new BattleScene(pokemons, sendback, user, sendBack2, false);
            this.Visibility = Visibility.Hidden;
            battleScene.ShowDialog();
            this.Close();
        }

        public SwitchPokemon(SerializationInfo info, StreamingContext context)
        {
            pokemons = (Pokemon[])info.GetValue("pokemons", typeof(Pokemon[]));
            sendback = (Pokemon[])info.GetValue("sendback", typeof(Pokemon[]));
            sendBack2 = (int)info.GetValue("sendback2", typeof(int));
            user = (int)info.GetValue("user", typeof(int));
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button sendo = (Button)sender;
            string num =(string)sendo.Name;
            int chosen = int.Parse(num[4].ToString())-1;
            if (pokemons[chosen].battleStats[1] > 0 && chosen != user)
            {
                BattleScene battleScene = new BattleScene(pokemons, sendback, chosen, sendBack2, shouldattack);
                this.Visibility = Visibility.Hidden;
                battleScene.ShowDialog();
                this.Close(); 
            }

        }
    }
}
